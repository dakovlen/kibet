(function ($, undefined) {
    $(document).ready(function () {
        $('.index-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000
        });

        $(".mobile-slider-news").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000
        });

        $('.goods-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000
        });


        $('.viewed-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4
                    }
                },

                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        dots: true
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        dots: true
                    }
                }
            ]
        });

        $('.tab-slider, .six-goods-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 850,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.top-sale-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.four-goods-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            centerMode: false,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
            speed: 1000,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2
                    }
                },

                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        dots: true
                    }
                }
            ]
        });

        $(".js-range-slider").ionRangeSlider({
            type: "double",
            min: 0,
            max: 3000,
            from: 0,
            to: 800,
            postfix: "грн."
        });

        $(".answer, .characteristics-link").click(function () {
            event.preventDefault();
            $(".review__form, .characteristic-box").toggleClass("open")
        });

        $(".special-product-slider").slick({
            vertical: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: '<button type="button" class="slick-next"><i class="icon-down-arrow"></i></button>',
            prevArrow: '<button type="button" class="slick-prev"><i class="icon-down-arrow"></i></button>',
            verticalSwiping: true
        });

        $(".product-slider").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            focusOnSelect: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        nextArrow: '<button type="button" class="slick-next"><i class="icon-next"></i></button>',
                        prevArrow: '<button type="button" class="slick-prev"><i class="icon-next"></i></button>',
                        arrows: true,
                        fade: false
                    }
                }
            ],
            asNavFor: '.product-slider-nav'
        });
        $(".product-slider-nav").slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.product-slider',
            dots: false,
            arrows: false,
            focusOnSelect: true
        });

        $(".search-mobile").click(function () {
            event.preventDefault();
            $(".input-search-box, .search-mobile").toggleClass("open")
        });

        $(".review-list__link").click(function () {
            event.preventDefault();
            $(".review-list").toggleClass("open")
        });

        $(".menu-icon-box").click(function() {
            $(".menu-mobile, .menu-icon-box, body").toggleClass("open");
        });

        $(".filter-mobile").click(function() {
            $(".filter-content").toggleClass("open");
        });

        $(".filter__head").click(function() {
            $(this).toggleClass("show")
        });

  /*      $(".tab-product-section .card-header").click(function() {
            $(this).toggleClass("open")
        });*/


        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });

        $("#input-id").rating();
    });
})(jQuery);
