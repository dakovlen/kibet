@extends('layouts.client')

@section('content')
    <section class="index-slider-section">
        <div class="container index-slider-container">
            <div class="index-slider">
                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>

                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>

                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>
            </div>
        </div>
    </section>
    <section class="top-section section">
        <div class="container">
            <h2 class="title-section">Топ продаж</h2>
            <div class="row mt-3">
                <div class="col-md-6 d-none  d-sm-block d-md-block d-lg-block d-xl-block">
                    <a href="/product">
                        <img src="{{ asset('assets/images/photo1.png') }}" alt="img">
                    </a>
                </div>
                <div class="col-md-6">
                    <div class="top-sale-slider base-slider">
                        <div class="goods-box">
                            <a href="/product" class="goods">
                                <span class="goods-visible">
                                    <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                         class="goods__img">

                                    <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                    <span class="goods-actions">
                                        <span class="goods__icon">
                                            <i class="icon-heart"></i>
                                        </span>
                                        <span class="goods__icon">
                                            <i class="icon-balance"></i>
                                        </span>
                                    </span>

                                    <span class="goods-price">
                                        <span class="price-action">
                                            <span class="price-action__new">
                                                320 <span>грн.</span>
                                            </span>
                                            <span class="price-action__old">
                                                520 <span>грн.</span>
                                            </span>
                                        </span>
                                        <span class="goods-price__cart icon-cart"></span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="goods-box">
                            <a href="/product" class="goods">
                                <span class="goods-visible">
                                    <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                         class="goods__img">

                                    <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                    <span class="goods-actions">
                                        <span class="goods__icon">
                                            <i class="icon-heart"></i>
                                        </span>
                                        <span class="goods__icon">
                                            <i class="icon-balance"></i>
                                        </span>
                                    </span>

                                    <span class="goods-price">
                                        <span class="price-action">
                                            <span class="price-action__new">
                                                320 <span>грн.</span>
                                            </span>
                                            <span class="price-action__old">
                                                520 <span>грн.</span>
                                            </span>
                                        </span>
                                        <span class="goods-price__cart icon-cart"></span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="goods-box">
                            <a href="/product" class="goods">
                                <span class="goods-visible">
                                    <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                         class="goods__img">

                                    <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                    <span class="goods-actions">
                                        <span class="goods__icon">
                                            <i class="icon-heart"></i>
                                        </span>
                                        <span class="goods__icon">
                                            <i class="icon-balance"></i>
                                        </span>
                                    </span>

                                    <span class="goods-price">
                                        <span class="price-action">
                                            <span class="price-action__new">
                                                320 <span>грн.</span>
                                            </span>
                                            <span class="price-action__old">
                                                520 <span>грн.</span>
                                            </span>
                                        </span>
                                        <span class="goods-price__cart icon-cart"></span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="goods-box">
                            <a href="/product" class="goods">
                                <span class="goods-visible">
                                    <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                         class="goods__img">

                                    <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                    <span class="goods-actions">
                                        <span class="goods__icon">
                                            <i class="icon-heart"></i>
                                        </span>
                                        <span class="goods__icon">
                                            <i class="icon-balance"></i>
                                        </span>
                                    </span>

                                    <span class="goods-price">
                                        <span class="price-action">
                                            <span class="price-action__new">
                                                320 <span>грн.</span>
                                            </span>
                                            <span class="price-action__old">
                                                520 <span>грн.</span>
                                            </span>
                                        </span>
                                        <span class="goods-price__cart icon-cart"></span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="goods-box">
                            <a href="/product" class="goods">
                                <span class="goods-visible">
                                    <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                         class="goods__img">

                                    <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                    <span class="goods-actions">
                                        <span class="goods__icon">
                                            <i class="icon-heart"></i>
                                        </span>
                                        <span class="goods__icon">
                                            <i class="icon-balance"></i>
                                        </span>
                                    </span>

                                    <span class="goods-price">
                                        <span class="price-action">
                                            <span class="price-action__new">
                                                320 <span>грн.</span>
                                            </span>
                                            <span class="price-action__old">
                                                520 <span>грн.</span>
                                            </span>
                                        </span>
                                        <span class="goods-price__cart icon-cart"></span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tab-product-section section">
        <div class="container">
            <h2 class="title-section">Новые поступления</h2>
            <div class="tab-product">
                <ul class="tab-product__nav nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#category1">Категория 1</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#category2">Категория 2</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#category3">Категория 3</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#category4">Категория 4</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#category5">Категория 5</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#category6">Категория 6</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#all-category">Все новинки</a>
                    </li>
                </ul>

                <div class="tab-product__content tab-content">
                    <div class="tab-pane fade show active" id="category1">
                        <div class="tab-slider base-slider">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag action">Акция</span>
                                        <img src="{{ asset('assets/images/product/product.png') }}" alt="goods img"
                                             class="goods__img">
                                        <span class="goods__title">Смартфон Huawei P Smart Z Sapphire</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag super-price">суперцена</span>
                                        <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag discount">скидка</span>
                                        <img src="{{ asset('assets/images/product/product3.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Водонагреватель проточный Holmer</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag present">подарок</span>
                                        <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag new">Новинка</span>
                                        <img src="{{ asset('assets/images/product/product5.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер TEFAL QUICKCHEF HB653838</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag bestseller">Хит продаж</span>
                                        <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>

                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag present">подарок</span>
                                        <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="category2">
                        Характеристики товара2...
                    </div>
                    <div class="tab-pane fade" id="category3">
                        Характеристики товара3...
                    </div>
                    <div class="tab-pane fade" id="category4">
                        Характеристики товара4...
                    </div>
                    <div class="tab-pane fade" id="category5">
                        Характеристики товара5...
                    </div>
                    <div class="tab-pane fade" id="category6">
                        Характеристики товара6...
                    </div>
                    <div class="tab-pane fade" id="all-category">
                        Характеристики товара7...
                    </div>
                </div>
            </div>

            <div class="accordion-product">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                    Категория 1
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                             data-parent="#accordion">
                            <div class="card-body">
                                <div class="tab-slider base-slider">
                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag action">Акция</span>
                                        <img src="{{ asset('assets/images/product/product.png') }}" alt="goods img"
                                             class="goods__img">
                                        <span class="goods__title">Смартфон Huawei P Smart Z Sapphire</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag super-price">суперцена</span>
                                        <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag discount">скидка</span>
                                        <img src="{{ asset('assets/images/product/product3.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Водонагреватель проточный Holmer</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag present">подарок</span>
                                        <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag new">Новинка</span>
                                        <img src="{{ asset('assets/images/product/product5.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер TEFAL QUICKCHEF HB653838</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag bestseller">Хит продаж</span>
                                        <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>

                                    <div class="goods-box">
                                        <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <span class="tag present">подарок</span>
                                        <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                    Категория 2
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Характеристики товара2...
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseThree" aria-expanded="false"
                                        aria-controls="collapseThree">
                                    Категория 3
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                             data-parent="#accordion">
                            <div class="card-body">
                                Характеристики товара3...
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading4">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse4" aria-expanded="false"
                                        aria-controls="collapse4">
                                    Категория 4
                                </button>
                            </h5>
                        </div>
                        <div id="collapse4" class="collapse" aria-labelledby="heading4"
                             data-parent="#accordion">
                            <div class="card-body">
                                Характеристики товара4...
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading5">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse5" aria-expanded="false"
                                        aria-controls="collapse5">
                                    Категория 5
                                </button>
                            </h5>
                        </div>
                        <div id="collapse5" class="collapse" aria-labelledby="heading5"
                             data-parent="#accordion">
                            <div class="card-body">
                                Характеристики товара5...
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header" id="heading6">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapse6" aria-expanded="false"
                                        aria-controls="collapse6">
                                    Все новинки
                                </button>
                            </h5>
                        </div>
                        <div id="collapse6" class="collapse" aria-labelledby="heading6"
                             data-parent="#accordion">
                            <div class="card-body">
                                Все новинки
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section home-news-section">
        <div class="container">
            <div class="row">
                <div class="col-md-9 d-none d-md-block d-lg-block d-xl-block">
                    <div class="d-flex justify-content-between align-items-center mb-3">
                        <h2 class="title-section">Новости, советы, статьи и обзоры</h2>
                        <a href="/news" class="link-news d-flex align-items-center">Все новости</a>
                    </div>
                    <div class="row no-gutters">
                        <div class="col-lg-4 col-md-6">
                            <a href="/single-news" class="news mini">
                                <span class="news__action">
                                    <img src="{{ asset('assets/images/blog/news.png') }}" alt="" class="news__img">
                                </span>

                                <span class="news__title">
                                    Характеристики HDD-накопителей
                                </span>
                            </a>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <a href="/single-news" class="news mini">
                                <span class="news__action">
                                    <img src="{{ asset('assets/images/blog/news2.png') }}" alt="" class="news__img">
                                </span>

                                <span class="news__title">
                                    Качественный и многофункциональный Zalman Z3 Plus
                                </span>
                            </a>
                        </div>

                        <div class="col-lg-4 d-none d-lg-block d-xl-block">
                            <a href="/single-news" class="news mini">
                                <span class="news__action">
                                    <img src="{{ asset('assets/images/blog/news3.png') }}" alt="" class="news__img">
                                </span>

                                <span class="news__title">
                                    Характеристики HDD-накопителей
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="home-news-text pb-4">
                        <p>
                            Приветствуем Вас в <b>интернет-магазине «Kibet»</b>, посвященному, в основном, ноутбукам,
                            планшетам,
                            телефонам и всем сопровождающим их комплектующим деталям, а также аудио-, ТВ и фототехнике.
                            Кроме
                            вышеперечисленной продукции, у нас покупатель найдет необходимое интернет-оборудование
                            (роутеры,
                            модемы), технику для дома и офиса, а также канцтовары.
                        </p>
                        <p>
                            Учитывая ритм, который задала современная жизнь, каждому человеку необходимо быть в Сети в
                            любое
                            время суток и в то же время сохранять мобильность. В этом поможет Вам ноутбук, который
                            позволяет по
                            максимуму использовать функции стационарного ПК, но в то же время быть мобильным. Для многих
                            из нас
                            подобная техника стала не просто отличным помощником, но и другом, на которого всегда можно
                            положиться. Именно поэтому мы выбираем себе качественные и надежные модели, которые смогут
                            поразить
                            своим функционалом и выполнить любую поставленную задачу. Как раз такие ноутбуки и
                            представлены в
                            нашем мультимаркете. Очень важно понимать, что подбор такого изделия в первую очередь
                            зависит от его
                            технических параметров и более производительные комплектующие приведут к повышению стоимости
                            планшета или ноутбука.
                        </p>
                        <p>
                            Главным достоинством, которым гордится наш мультимаркет, является наличие
                            высококачественного
                            ассортимента компьютерной техники, возможность приобретения товара в режиме онлайн по
                            отличной и
                            выгодной для клиента цене и предоставление гибкого графика доставки приобретенной продукции.
                            В
                            ассортимент включена не только простая по оснащению техника, но и бизнес версии мобильных
                            средств,
                            которые обладают высокой функциональностью и повышенной ударопрочностью.
                        </p>
                        <p>
                            Наш сайт позволит Вам подобрать наиболее оптимальную модель планшета и ознакомит со всеми
                            новинками
                            в области компьютерной техники. В каждом из разделов можно встретить большое количество всем
                            знакомых брендов, которые зарекомендовали себя только с лучшей стороны.
                        </p>
                        <p>
                            Вы можете самостоятельно совершить заказ в интернет-магазине, после чего в течение
                            нескольких дней
                            Вы получите свой товар. Подобные условия сэкономят время, и Вы получите приобретенный заказ
                            в
                            целости и сохранности. Каждая единицы техники, которую Вы можете найти в нашем
                            интернет-мультимаркете «Kibet», обладает фирменной гарантией, которую предоставляет её
                            производитель. Для оплаты товара можно воспользоваться наличными или же оформить покупку в
                            рассрочку
                            или кредит. По любым вопросам Вы можете обратиться к консультантам по указанному телефону.
                        </p>
                    </div>
                </div>
                <div class="col-md-3 ">
                    @yield('sidebar', View::make('client.partial.sidebar'))
                </div>
            </div>
        </div>
    </section>
@endsection
