@extends('layouts.client')

@section('content')
    <a href="#" class="category-action mobile">
        <img src="{{ asset('assets/images/banner.png') }}" alt="">
    </a>
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Комплектующие для ПК</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="category-container">
        <div class="container">
            <h2 class="title">Комплектующие для ПК</h2>
            <div class="row">
                <div class="col-md-3 flex20">
                    <div class="filter">
                        <div class="filter-content">
                            <ul class="category-link-list">
                                <li>
                                    <a href="#">Процессоры</a>
                                </li>

                                <li>
                                    <a href="#">Материнские платы</a>
                                </li>

                                <li>
                                    <a href="#">Оперативная память</a>
                                </li>

                                <li>
                                    <a href="#">Жесткие диски</a>
                                </li>

                                <li>
                                    <a href="#">Корпуса</a>
                                </li>

                                <li>
                                    <a href="#">Блоки питания</a>
                                </li>

                                <li>
                                    <a href="#">Системы охлаждения</a>
                                </li>

                                <li>
                                    <a href="#">Видеокарты</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-md-9 flex80">
                    <a href="#" class="category-action">
                        <img src="{{ asset('assets/images/banner.png') }}" alt="">
                    </a>

                    <div class="row">
                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Процессы</span>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category1.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Материнские платы</span>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category2.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Оперативная память</span>
                                <ul class="category__list">
                                    <li><a href="#">для ПК</a></li>
                                    <li><a href="#">для ноутбука</a></li>
                                </ul>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category3.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Жесткие диски</span>
                                <ul class="category__list">
                                    <li><a href="#">для ПК</a></li>
                                    <li><a href="#">для ноутбука</a></li>
                                    <li><a href="#">внешние жесткие диски</a></li>
                                    <li><a href="#">SSD-накопители</a></li>
                                    <li><a href="#">карманы для жестких дисков</a></li>
                                    <li><a href="#">оптические приводы</a></li>
                                </ul>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category5.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Корпуса</span>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category6.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Блоки питания</span>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category7.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Системы охлаждения</span>
                                <ul class="category__list">
                                    <li><a href="#">для процессора</a></li>
                                    <li><a href="#">для корпуса</a></li>
                                    <li><a href="#">для видеокарты</a></li>
                                    <li><a href="#">для жесткого диска</a></li>
                                    <li><a href="#">термопаста</a></li>
                                </ul>
                            </a>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <a href="/catalog" class="category">
                                <img src="{{ asset('assets/images/category/category8.png') }}" alt="category img" class="category__img">
                                <span class="category__title">Видеокарты</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section pb-5">
        <div class="container">
            <h2 class="title-section">Топ продаж</h2>

            @yield('sidebar', View::make('client.partial.six_goods_slider'))
        </div>
    </section>
@endsection
