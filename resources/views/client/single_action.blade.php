@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">К акционным SSD-накопителям Western Digital –
                        сертификат
                    </li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="actions-container pb-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <img src="{{ asset('assets/images/action.png') }}" alt="action images" class="stock__img">
                </div>
                <div class="col-lg-6">
                    <div class="stock">
                        <div class="stock__title">Акция! К акционным ноутбукам
                            с накопителямы Western Digital – сертификат Kibet до 2000 грн в подарок!
                        </div>

                        <div class="stock__end">До конца осталось:</div>
                        <div class="stock__date">
                            12:07:55:35
                        </div>
                        <div class="stock__period">Период проведения акции: с 11 марта по 25 марта 2020 года</div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-3 flex20">
                    <div class="filter">
                        <div class="filter-mobile">
                            <i class="icon-filter filter-mobile__icon"></i>
                            Фильтр
                        </div>
                        <div class="filter-content">
                            <div class="stock-header">
                                <h3 class="stock__active-title">Все товары акции</h3>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse2"
                                        aria-expanded="false" aria-controls="collapse2">
                                    Цена
                                </button>

                                <div class="collapse" id="collapse2">
                                    <div class="filter__content pt-4 pb-4">
                                        <input type="text" class="js-range-slider" name="my_range" value=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse3"
                                        aria-expanded="false" aria-controls="collapse3">
                                    Производитель
                                </button>

                                <div class="collapse" id="collapse3">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Asus</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Lenovo</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>ACER</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Hewlett Packard</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Dell</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse4"
                                        aria-expanded="false" aria-controls="collapse4">
                                    Цвет
                                </button>

                                <div class="collapse" id="collapse4">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>белый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>золотой</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Церный</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>розовый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>коричневый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>красный</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse5"
                                        aria-expanded="false" aria-controls="collapse5">
                                    Семейство CPU
                                </button>

                                <div class="collapse" id="collapse5">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>FX-</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A10</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A4</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A6</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Celeron</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                            <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse6"
                                    aria-expanded="false" aria-controls="collapse6">
                                Объем RAM
                            </button>

                            <div class="collapse" id="collapse6">
                                <div class="filter__content">
                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>8Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>12Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>12Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>32Gb</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 flex80">
                    <div class="stock-header">
                        <div class="d-block d-md-flex d-lg-flex d-lg-block justify-content-between align-items-center">
                            <h3>Товары, которые участвуют</h3>
                            <a href="#" class="ml-3">Условия акции</a>
                        </div>

                        <div class="share">
                            <a href="#" class="share__link icon-fb"></a>
                            <a href="#" class="share__link icon-youtube"></a>
                            <a href="#" class="share__link icon-inst"></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-12 d-lg-none d-xl-none">
                            <div class="text-center mt-4 mb-3">
                                <a href="#" class="more-news">Показать еще <span>32</span> товара </a>
                            </div>
                        </div>

                        <div class="col-12 d-lg-none d-xl-none">
                            <ul class="pagination mt-4">
                                <li class="page-item"><a class="page-link" href="/single-action">1</a></li>
                                <li class="page-item active"><a class="page-link" href="/single-action">2</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">3</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">4</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">...</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">32</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action"> > </a></li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <div class="stock-text">
                                <p>
                                    Каждый покупатель акционных ноутбуков с накопителями Western Digital получает в
                                    подарок сертификат Kibet до 2000 грн*!
                                </p>
                                <span>
                                    Период проведения акции: 11 марта — 25 марта 2020 г.
                                </span>

                                <span>
                                    Спешите, количество подарков ограничено!
                                </span>
                            </div>
                        </div>

                        <div class="col-12 sidebar d-lg-none d-xl-none">
                            <div class="sidebar__row">
                                <div class="sidebar__title">
                                    <span>Подписаться на рассылку</span>
                                </div>
                                <div class="sidebar__mail">
                                    <input type="email" placeholder="Ваш E-mail">
                                    <i class="icon-mail icon"></i>
                                </div>
                            </div>
                            <div class="sidebar__row">
                                <div class="sidebar__title">
                                    <span>Мы в соц. сетях</span>
                                </div>
                                <div class="social">
                                    <a href="#" class="social__icon icon-fb"></a>
                                    <a href="#" class="social__icon icon-youtube"></a>
                                    <a href="#" class="social__icon icon-inst"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
