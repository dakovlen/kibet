@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Ноутбуки</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="catalog-container">
        <div class="container">
            <h2 class="title">Ноутбуки</h2>
            <div class="row">
                <div class="col-md-3 flex20">
                    <div class="filter">
                        <div class="filter-mobile">
                            <i class="icon-filter filter-mobile__icon"></i>
                            Фильтр
                        </div>
                        <div class="filter-content">
                            <div class="filter-row">
                                <div class="filter-selected">
                                    <div class="filter-selected__title">
                                        Вы выбрали
                                    </div>

                                    <div class="filter-selected__content">
                                        <a href="#" class="filter-link">
                                            <i class="filter-link__icon icon-times-solid"></i>
                                            15,6
                                        </a>
                                        <a href="#" class="filter-link">
                                            <i class="filter-link__icon icon-times-solid"></i>
                                            Матовый
                                        </a>
                                        <a href="#" class="filter-link">
                                            <i class="filter-link__icon icon-times-solid"></i>
                                            500Gb
                                        </a>

                                        <a href="#" class="filter-link__clear">Сбросить</a>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head"
                                        type="button"
                                        data-toggle="collapse"
                                        data-target="#collapse2"
                                        aria-expanded="false"
                                        aria-controls="collapse2">
                                    Цена
                                </button>

                                <div class="collapse" id="collapse2">
                                    <div class="filter__content pt-3 pb-5 overflow-hidden">
                                        <input type="text" class="js-range-slider" name="my_range" value="" />
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse3"
                                        aria-expanded="false" aria-controls="collapse3">
                                    Производитель
                                </button>

                                <div class="collapse" id="collapse3">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Asus</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Lenovo</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>ACER</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Hewlett Packard</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Dell</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse4"
                                        aria-expanded="false" aria-controls="collapse4">
                                    Цвет
                                </button>

                                <div class="collapse" id="collapse4">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>белый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>золотой</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Церный</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>розовый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>коричневый</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>красный</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse5"
                                        aria-expanded="false" aria-controls="collapse5">
                                    Семейство CPU
                                </button>

                                <div class="collapse" id="collapse5">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>FX-</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A10</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A4</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A6</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Celeron</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                            <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse6"
                                    aria-expanded="false" aria-controls="collapse6">
                                Объем RAM
                            </button>

                            <div class="collapse" id="collapse6">
                                <div class="filter__content">
                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>8Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>12Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>12Gb</span>
                                    </label>

                                    <label class="custom-checkbox">
                                        <input type="checkbox">
                                        <span>32Gb</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 flex80">
                    <div class="sort-row">
                        Сортировать:
                        <div class="dropdown ml-3">
                            <button class="dropdown-sort dropdown-toggle" type="button" id="dropdownSort"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                по популярности
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownSort">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box inactive">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/action.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>

                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                        <span class="report">
                                            <span>Нет в наличии</span>
                                            <span class="report__icon icon-mail"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <div class="goods">
                                    <span class="goods-visible">

                                        <span class="goods-slider">
                                            <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img">
                                            <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img">
                                            <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img">
                                            <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img">
                                        </span>

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                        <span>Диагональ дисплея: 13,3"</span>
                                        <span>Дисплей: Матовый экран</span>
                                        <span>Тип процессора: Intel Core M7-6Y7 </span>
                                        <span>Оперативная память: 8 ГБ</span>
                                        <span>Жесткий диск SSD: 512 Гб</span>
                                        <span>Граф. адаптер: Intel HD Graphics</span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/photo2.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-4">
                            <div class="goods-box">
                                <a href="/product" class="goods">
                                    <span class="goods-visible">
                                        <img src="{{ asset('assets/images/laptop1.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                                        <span class="goods-actions">
                                            <span class="goods__rating">
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                                <span class="icon-star"></span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-message"></i>
                                                <span>12</span>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>
                                        <span class="action-code">Код:4079100</span>
                                        <span class="goods-price">
                                            <span class="goods-price__number">
                                                520 <span>грн.</span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    <span class="goods-hover">
                                    <span>Диагональ дисплея: 13,3"</span>
                                    <span>Дисплей: Матовый экран</span>
                                    <span>Тип процессора: Intel Core M7-6Y7 </span>
                                    <span>Оперативная память: 8 ГБ</span>
                                    <span>Жесткий диск SSD: 512 Гб</span>
                                    <span>Граф. адаптер: Intel HD Graphics</span>
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="text-center mt-3 mt-md-5 mt-lg-5 mt-xl-5 mb-3 mb-md-5 mb-lg-5 mb-xl-5">
                        <a href="#" class="more-news">Показать еще <span>32</span> товара </a>
                    </div>

                    <ul class="pagination">
                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                        <li class="page-item"><a class="page-link" href="#">32</a></li>
                        <li class="page-item"><a class="page-link" href="#"> > </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h2 class="title-section">Просмотренные товары</h2>
            @yield('sidebar', View::make('client.partial.six_goods_slider'))

            <div class="mobile-sidebar-social">
                <div class="sidebar__title">
                    <span>Мы в соц. сетях</span>
                </div>
                <div class="social">
                    <a href="#" class="social__icon icon-fb"></a>
                    <a href="#" class="social__icon icon-youtube"></a>
                    <a href="#" class="social__icon icon-inst"></a>
                </div>
            </div>
        </div>
    </section>

    <section class="catalog-text section pb-5">
        <div class="container">
            <p>
                Приветствуем Вас в <b>интернет-магазине «Kibet»,</b> посвященному, в основном, ноутбукам, планшетам, телефонам
                и всем сопровождающим их комплектующим деталям, а также аудио-, ТВ и фототехнике. Кроме
                вышеперечисленной продукции, у нас покупатель найдет необходимое интернет-оборудование (роутеры,
                модемы), технику для дома и офиса, а также канцтовары.
            </p>
            <p>
                Учитывая ритм, который задала современная жизнь, каждому человеку необходимо быть в Сети в любое время
                суток и в то же время сохранять мобильность. В этом поможет Вам ноутбук, который позволяет по максимуму
                использовать функции стационарного ПК, но в то же время быть мобильным. Для многих из нас подобная
                техника стала не просто отличным помощником, но и другом, на которого всегда можно положиться. Именно
                поэтому мы выбираем себе качественные и надежные модели, которые смогут поразить своим функционалом и
                выполнить любую поставленную задачу. Как раз такие ноутбуки и представлены в нашем мультимаркете. Очень
                важно понимать, что подбор такого изделия в первую очередь зависит от его технических параметров и более
                производительные комплектующие приведут к повышению стоимости планшета или ноутбука.
            </p>
            <p>
                Главным достоинством, которым гордится наш мультимаркет, является наличие высококачественного
                ассортимента компьютерной техники, возможность приобретения товара в режиме онлайн по отличной и
                выгодной для клиента цене и предоставление гибкого графика доставки приобретенной продукции. В
                ассортимент включена не только простая по оснащению техника, но и бизнес версии мобильных средств,
                которые обладают высокой функциональностью и повышенной ударопрочностью.
            </p>
            <p>
                Наш сайт позволит Вам подобрать наиболее оптимальную модель планшета и ознакомит со всеми новинками в
                области компьютерной техники. В каждом из разделов можно встретить большое количество всем знакомых
                брендов, которые зарекомендовали себя только с лучшей стороны.
            </p>
            <p>
                Вы можете самостоятельно совершить заказ в интернет-магазине, после чего в течение нескольких дней Вы
                получите свой товар. Подобные условия сэкономят время, и Вы получите приобретенный заказ в целости и
                сохранности. Каждая единицы техники, которую Вы можете найти в нашем интернет-мультимаркете «Kibet»,
                обладает фирменной гарантией, которую предоставляет её производитель. Для оплаты товара можно
                воспользоваться наличными или же оформить покупку в рассрочку или кредит. По любым вопросам Вы можете
                обратиться к консультантам по указанному телефону.
            </p>
        </div>
    </section>
@endsection
