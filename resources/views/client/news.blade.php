@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Блог</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="news-container">
        <div class="container">
            <h2 class="title">Блог</h2>

            <div class="mobile-slider-news">
                <a href="/single-news" class="news">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/blog1.png') }}" alt="" class="news__img">
                        </span>

                    <span class="news__title">
                            Ловкий помощник для автолюбителя: как выбрать хороший автомобильный пылесос
                        </span>
                </a>

                <a href="/single-news" class="news">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/blog2.png') }}" alt="" class="news__img">
                        </span>

                    <span class="news__title">
                            Ловкий помощник для автолюбителя: как выбрать хороший автомобильный пылесос
                        </span>
                </a>

                <a href="/single-news" class="news">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/blog3.png') }}" alt="" class="news__img">
                        </span>

                    <span class="news__title">
                            Ловкий помощник для автолюбителя: как выбрать хороший автомобильный пылесос
                        </span>
                </a>
            </div>

            <div class="row">
                <div class="col-md-6 d-none d-md-block d-lg-block d-xl-block">
                    <a href="/single-news" class="news">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/blog1.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news__title">
                            Ловкий помощник для автолюбителя: как выбрать хороший автомобильный пылесос
                        </span>
                    </a>
                </div>

                <div class="col-md-6 col-lg-6 d-none d-md-block d-lg-block d-xl-block">
                    <a href="/single-news" class="news">
                        <span class="news__action">
                            <span class="tag advice">советы</span>
                            <img src="{{ asset('assets/images/blog/blog2.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news__title">
                            Рассказываем о популярных видах покрытия подошвы утюга: какие они бывают и чем отличаются
                        </span>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 d-none d-md-block d-lg-block d-xl-block">
                    <a href="/single-news" class="news mini">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/img1.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news__title">
                            Как красиво упаковать подарок? 10 идей
                        </span>
                    </a>
                </div>

                <div class="col-md-6 col-lg-4 d-none d-md-block d-lg-block d-xl-block">
                    <a href="/single-news" class="news mini">
                        <span class="news__action">
                            <span class="tag advice">советы</span>
                            <img src="{{ asset('assets/images/blog/img3.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news__title">
                            Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                            Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                        </span>
                    </a>
                </div>

                <div class="col-md-4 d-none d-lg-block d-xl-block">
                    <a href="/single-news" class="news mini">
                        <span class="news__action">
                            <span class="tag tag-news">новости</span>
                            <img src="{{ asset('assets/images/blog/blog3.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news__title">
                            Кодировка спутниковых каналов в Украине: что нужно знать и как смотреть
                        </span>
                    </a>
                </div>
            </div>

            <div class="row pb-4">
                <div class="col-md-3 order-2 order-md-0">
                    <div class="accordion-news-desc">
                        <div class="accordion-news-box mb-5">
                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse1"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 1
                                </button>

                                <div class="collapse" id="collapse1">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse2"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 2
                                </button>

                                <div class="collapse" id="collapse2">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse3"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 3
                                </button>

                                <div class="collapse" id="collapse3">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @yield('sidebar', View::make('client.partial.sidebar'))
                </div>

                <div class="col-md-9">
                    <div class="row-link">
                        <a href="#" class="active">Все</a>
                        <a href="#">Новости</a>
                        <a href="#">Обзоры</a>
                        <a href="#">Советы</a>
                    </div>

                    <div class="accordion-news-mobile">
                        <div class="accordion-news-box mb-4">
                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse1"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 1
                                </button>

                                <div class="collapse" id="collapse1">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse2"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 2
                                </button>

                                <div class="collapse" id="collapse2">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="accordion-news">
                                <button class="accordion-news__head" type="button" data-toggle="collapse" data-target="#collapse3"
                                        aria-expanded="false" aria-controls="collapseExample">
                                    Категория 3
                                </button>

                                <div class="collapse" id="collapse3">
                                    <ul class="accordion-news__list">
                                        <li><a href="#">Подкатегория 2 (9)</a></li>
                                        <li><a href="#">Подкатегория 5 (20)</a></li>
                                        <li><a href="#">Подкатегория 7 (7)</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="" class="news news-horizontal">
                        <span class="news__action">
                            <span class="tag tag-news">новости</span>
                            <img src="{{ asset('assets/images/blog/blog4.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news-info">
                            <span class="news__title">
                                Стало известно, когда PS5 поступит в продажу: дата и цена
                            </span>
                            <span class="news-author">
                                <span>16/02/2020</span>
                                <span>Автор</span>
                            </span>
                            <span class="news-text">
                                Игровая приставка следующего поколения Sony PlayStation 5
                                появится в магазинах раньше, чем мы того ожидали. Стали
                                известны сроки поступления в продажу и ее цена. Информация
                                появилась на портале 4Chan с грифом.
                            </span>
                        </span>
                    </a>

                    <a href="" class="news news-horizontal">
                        <span class="news__action">
                            <span class="tag overview">обзоры</span>
                            <img src="{{ asset('assets/images/blog/blog5.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news-info">
                            <span class="news__title">
                                Индукционная поверхность Candy CIC642: воплощение функциональности и простоты.
                            </span>
                            <span class="news-author">
                                <span>12/01/20</span>
                                <span>Автор</span>
                            </span>
                            <span class="news-text">
                                Игровая приставка следующего поколения Sony PlayStation 5
                                появится в магазинах раньше, чем мы того ожидали. Стали
                                известны сроки поступления в продажу и ее цена. Информация
                                появилась на портале 4Chan с грифом.
                            </span>
                        </span>
                    </a>

                    <a href="" class="news news-horizontal">
                        <span class="news__action">
                            <span class="tag advice">советы</span>
                            <img src="{{ asset('assets/images/blog/blog4.png') }}" alt="" class="news__img">
                        </span>

                        <span class="news-info">
                            <span class="news__title">
                                Стало известно, когда PS5 поступит в продажу: дата и цена
                            </span>
                            <span class="news-author">
                                <span>16/02/2020</span>
                                <span>Автор</span>
                            </span>
                            <span class="news-text">
                                Игровая приставка следующего поколения Sony PlayStation 5
                                появится в магазинах раньше, чем мы того ожидали. Стали
                                известны сроки поступления в продажу и ее цена. Информация
                                появилась на портале 4Chan с грифом.
                            </span>
                        </span>
                    </a>

                    <div class="text-center mt-4 mb-4">
                        <a href="#" class="more-news">Больше статей</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
