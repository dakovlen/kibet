@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item"><a href="#">Блог</a></li>
                    <li class="breadcrumb-item"><a href="#">Новости</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Стало известно, когда PS5 поступит в продажу:
                        дата и цена
                    </li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="article-container">
        <div class="container">
            <div class="row-link">
                <a href="#" class="active">Новости</a>
                <a href="#">Обзоры</a>
                <a href="#">Советы</a>
            </div>
            <h2 class="title">Стало известно, когда PS5 поступит в продажу: дата и цена</h2>
            <div class="news-author">
                <span>16/02/2020</span>
                <span>Автор</span>
            </div>
            <div class="article-content pb-5">
                <div class="share">
                    <a href="#" class="share__link icon-fb"></a>
                    <a href="#" class="share__link icon-youtube"></a>
                    <a href="#" class="share__link icon-inst"></a>
                </div>
                <div class="row">
                    <div class="col-lg-9">
                        <img src="{{ asset('assets/images/blog/main_blog.png') }}" alt="img" class="news__img main-img">

                        <p>
                            Игровая приставка следующего поколения Sony PlayStation 5 появится в магазинах раньше,
                            чем мы того ожидали. Стали известны сроки поступления в продажу и ее цена. Информация
                            появилась на портале 4Chan с грифом «совершенно секретно». Указывается, что она получена
                            из внутреннего документа Sony Interactive Entertainment, который в Интернет слил один из
                            сотрудников компании.
                        </p>
                        <p>
                            Sony PlayStation 5 должна поступить в продажу уже в октябре, а не в ноябре-декабре, как
                            предполагалось ранее. Официальная рекомендованная цена новинки – $499 или €499, в
                            зависимости от региона. Все подробности об игровой приставке следующего поколения Sony
                            даст на конференции PlayStation Meeting, которая, если верить утечкам, пройдет 5 февраля
                            2020 года
                        </p>
                        <p>
                            Если верить этим данным, игровую консоль следующего поколения анонсируют 5 февраля 2020
                            года на конференции PlayStation Meeting (США). PS4 показали на PlayStation Meeting 20
                            февраля 2013 года. На грядущем мероприятии производитель раскроет все карты: дизайн
                            приставки и контроллеров, интерфейс и домашний экран, функции и технические
                            характеристики, а также перечень эксклюзивных игр.
                        </p>

                        <div class="mt-4 mb-4 ">
                            <h2 class="title-section mb-3">Товары из этой статьи</h2>

                            <div class="four-goods-slider base-slider">
                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                        <span class="tag bestseller">Хит продаж</span>
                                        <img src="{{ asset('assets/images/product/product5.png') }}" alt="goods img"
                                             class="goods__img">

                                        <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                        <span class="goods-actions">
                                            <span class="goods__icon">
                                                <i class="icon-heart"></i>
                                            </span>
                                            <span class="goods__icon">
                                                <i class="icon-balance"></i>
                                            </span>
                                        </span>

                                        <span class="goods-price">
                                            <span class="price-action">
                                                <span class="price-action__new">
                                                    320 <span>грн.</span>
                                                </span>
                                                <span class="price-action__old">
                                                    520 <span>грн.</span>
                                                </span>
                                            </span>
                                            <span class="goods-price__cart icon-cart"></span>
                                        </span>
                                    </span>
                                    </a>
                                </div>

                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                            <span class="tag new">новинка</span>
                                            <img src="{{ asset('assets/images/product/product3.png') }}" alt="goods img"
                                                 class="goods__img">

                                            <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                            <span class="goods-actions">
                                                <span class="goods__icon">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span class="goods__icon">
                                                    <i class="icon-balance"></i>
                                                </span>
                                            </span>

                                            <span class="goods-price">
                                                <span class="price-action">
                                                    <span class="price-action__new">
                                                        320 <span>грн.</span>
                                                    </span>
                                                    <span class="price-action__old">
                                                        520 <span>грн.</span>
                                                    </span>
                                                </span>
                                                <span class="goods-price__cart icon-cart"></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                            <span class="tag present">подарок</span>
                                            <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                                                 class="goods__img">

                                            <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                                            <span class="goods-actions">
                                                <span class="goods__icon">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span class="goods__icon">
                                                    <i class="icon-balance"></i>
                                                </span>
                                            </span>

                                            <span class="goods-price">
                                                <span class="price-action">
                                                    <span class="price-action__new">
                                                        320 <span>грн.</span>
                                                    </span>
                                                    <span class="price-action__old">
                                                        520 <span>грн.</span>
                                                    </span>
                                                </span>
                                                <span class="goods-price__cart icon-cart"></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                            <span class="tag discount">скидка</span>
                                            <img src="{{ asset('assets/images/product/product3.png') }}" alt="goods img" class="goods__img">

                                            <span class="goods__title">Водонагреватель проточный Holmer</span>

                                            <span class="goods-actions">
                                                <span class="goods__icon">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span class="goods__icon">
                                                    <i class="icon-balance"></i>
                                                </span>
                                            </span>

                                            <span class="goods-price">
                                                <span class="price-action">
                                                    <span class="price-action__new">
                                                        320 <span>грн.</span>
                                                    </span>
                                                    <span class="price-action__old">
                                                        520 <span>грн.</span>
                                                    </span>
                                                </span>
                                                <span class="goods-price__cart icon-cart"></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                            <span class="tag bestseller">Хит продаж</span>
                                            <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                                                 class="goods__img">

                                            <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                            <span class="goods-actions">
                                                <span class="goods__icon">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span class="goods__icon">
                                                    <i class="icon-balance"></i>
                                                </span>
                                            </span>

                                            <span class="goods-price">
                                                <span class="price-action">
                                                    <span class="price-action__new">
                                                        320 <span>грн.</span>
                                                    </span>
                                                    <span class="price-action__old">
                                                        520 <span>грн.</span>
                                                    </span>
                                                </span>
                                                <span class="goods-price__cart icon-cart"></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>

                                <div class="goods-box">
                                    <a href="/product" class="goods">
                                        <span class="goods-visible">
                                            <span class="tag bestseller">Хит продаж</span>
                                            <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                                                 class="goods__img">

                                            <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                                            <span class="goods-actions">
                                                <span class="goods__icon">
                                                    <i class="icon-heart"></i>
                                                </span>
                                                <span class="goods__icon">
                                                    <i class="icon-balance"></i>
                                                </span>
                                            </span>

                                            <span class="goods-price">
                                                <span class="price-action">
                                                    <span class="price-action__new">
                                                        320 <span>грн.</span>
                                                    </span>
                                                    <span class="price-action__old">
                                                        520 <span>грн.</span>
                                                    </span>
                                                </span>
                                                <span class="goods-price__cart icon-cart"></span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="news-comments-form">
                            <div class="comments-head">
                                <h2 class="title-section">Комментарии</h2>
                                <div class="enter-through">
                                    Войти через:
                                    <a href="#" class="enter-through__link icon-google"></a>
                                    <a href="#" class="enter-through__link icon-fb"></a>
                                </div>
                            </div>
                            <form action="/" class="row mt-3">
                                <div class="col-md-6">
                                    <input type="text" placeholder="Ваше имя">
                                </div>
                                <div class="col-md-6">
                                    <input type="email" placeholder="Email">
                                </div>
                                <div class="col-md-12">
                                    <textarea name="" id="" class="mt-4" placeholder="Комментарий" cols="30"
                                              rows="7"></textarea>
                                </div>
                                <div class="col-md-12 comment-btn mt-4 mb-4">
                                    <button class="btn">Комментировать</button>
                                    <div class="mr-5">
                                        Уже есть аккаунт?
                                        <a href="#">Войти под своим именем</a>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="news-comments">
                            <div class="news-comments__head">
                                <div class="news-comments__title">Вася Петечкин</div>
                                <div class="news-comments__date">12 марта 2020</div>
                            </div>
                            <div class="news-comments__text">
                                Через пару дней после покупки пропал звук на динамиках, вместо звука теперь слышны тихие
                                щелчки. В наушниках звук есть. Что делать не знаю, все драйвера переустановил и
                                диагностики которые Виндоус предлагает сделал, пишет все ок, а динамики не работают.
                                Осталось только ОС переустановить, если не появится то буду думать о возврате
                            </div>
                            <div class="news-comments__bottom">
                                <div class="answer-box">
                                    <a href="#" class="answer">Ответить</a>
                                    <div>5 ответов</div>
                                </div>
                                <div class="like-box">
                                    <a href="#" class="like-link like">
                                        <i class="icon-like"></i>
                                        5
                                    </a>
                                    <a href="#" class="like-link dislike">
                                        <i class="icon-dislike"></i>
                                        5
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-4 mb-4">
                            <a href="#" class="more-news">Все комментарии <span>13</span>  </a>
                        </div>

                        <h2 class="title-section">Читайте также</h2>
                        <div class="row">
                            <div class="col-md-6">
                                <a href="/single-news" class="news mini">
                                    <span class="news__action">
                                        <span class="tag advice">советы</span>
                                        <img src="{{ asset('assets/images/blog/img1.png') }}" alt="" class="news__img">
                                    </span>

                                    <span class="news__title">
                                    Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                                    Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                                </span>
                                </a>
                            </div>

                            <div class="col-md-6">
                                <a href="/single-news" class="news mini">
                                    <span class="news__action">
                                        <span class="tag advice">советы</span>
                                        <img src="{{ asset('assets/images/blog/img3.png') }}" alt="" class="news__img">
                                    </span>

                                    <span class="news__title">
                                    Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                                    Какой телефон был первый: с фронталкой, 4К, стереозвуком, сенсорным экраном и сканером...
                                </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 d-none d-lg-block d-xl-block">
                        <div class="banner">
                            <img src="{{ asset('assets/images/banner_left.png') }}" alt="img">
                        </div>

                        @yield('sidebar', View::make('client.partial.sidebar'))
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
