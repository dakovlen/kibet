@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Kibet</a></li>
                    <li class="breadcrumb-item"><a href="#">ТВ, Аудио, Фото</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Наушники</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="product-container">
        <div class="container">
            <h2 class="title">Гарнитура Bluetooth Gemix BH-07 Gold, Bluetooth V3.0+EDR, накладные</h2>
            <h5>Код товара: <b>4079100</b></h5>

            <div class="product-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="about-product-tab" data-toggle="tab" href="#about-product" role="tab"
                           aria-controls="about-product" aria-selected="true">О товаре</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="characteristics-tab" data-toggle="tab" href="#characteristics" role="tab"
                           aria-controls="characteristics-tab" aria-selected="false">Характеристики</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab"
                           aria-controls="reviews" aria-selected="false">Отзывы</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="buy-together-tab" data-toggle="tab" href="#buy-together" role="tab"
                           aria-controls="buy-together" aria-selected="false">Вместе покупают</a>
                    </li>

                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="about-product" role="tabpanel" aria-labelledby="about-product">
                        @yield('about-product', View::make('client.product_tab.about'))
                    </div>
                    <div class="tab-pane fade" id="characteristics" role="tabpanel" aria-labelledby="characteristics-tab">
                        @yield('characteristics', View::make('client.product_tab.characteristics'))
                    </div>
                    <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews">
                        @yield('reviews', View::make('client.product_tab.reviews'))

                    </div>
                    <div class="tab-pane fade buy-together" id="buy-together" role="tabpanel" aria-labelledby="buy-together">
                        @yield('buy_together', View::make('client.product_tab.buy_together'))</div>
                </div>
            </div>
        </div>
    </section>

    <section class="section pb-5">
        <div class="container">
            <h2 class="title-section mb-3">Просмотренные товары</h2>
            @yield('sidebar', View::make('client.partial.six_goods_slider'))
        </div>
    </section>
@endsection
