@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/single-action">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Корзина</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="card-container pb-5">
        <div class="container">
            <h2 class="title">Корзина</h2>

            <div class="basket-box">
                <div class="basket">
                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="" class="basket__img">
                        <div class="basket__info">
                            <div class="basket__title">Название продукта название продукта</div>
                            <div class="basket__code">Код товара <b>4079100</b></div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <div class="basket__price">2768грн</div>

                        <div class="basket__count">
                            <div class="number">
                                <span class="minus">-</span>
                                <input type="text" value="1" class="number__input"/>
                                <span class="plus">+</span>
                            </div>
                        </div>

                        <div class="basket__price">2768грн</div>

                        <div class="basket__cancel">
                            <i class="icon-times-circle-regular"></i>
                        </div>
                    </div>
                </div>

                <div class="basket">
                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="" class="basket__img">
                        <div class="basket__info">
                            <div class="basket__title">Название продукта название продукта</div>
                            <div class="basket__code">Код товара <b>4079100</b></div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <div class="basket__price">2768грн</div>

                        <div class="basket__count">
                            <div class="number">
                                <span class="minus">-</span>
                                <input type="text" value="1" class="number__input"/>
                                <span class="plus">+</span>
                            </div>
                        </div>

                        <div class="basket__price">2768грн</div>

                        <div class="basket__cancel">
                            <i class="icon-times-circle-regular"></i>
                        </div>
                    </div>
                </div>

                <div class="basket">
                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="" class="basket__img">
                        <div class="basket__info">
                            <div class="basket__title">Название продукта название продукта</div>
                            <div class="basket__code">Код товара <b>4079100</b></div>
                        </div>
                    </div>

                    <div class="d-flex align-items-center justify-content-center d-md-block">
                        <div class="basket__price">2768грн</div>

                        <div class="basket__count">
                            <div class="number">
                                <span class="minus">-</span>
                                <input type="text" value="1" class="number__input"/>
                                <span class="plus">+</span>
                            </div>
                        </div>

                        <div class="basket__price">2768грн</div>

                        <div class="basket__cancel">
                            <i class="icon-times-circle-regular"></i>
                        </div>
                    </div>
                </div>

                <div class="basket__bottom">
                    <div class="basket__bottom--input">
                        <span>Ввести промокод</span>
                        <div class="input-basket-box">
                            <input type="text">
                            <button>
                                <i class="icon-check-double-solid"></i>
                            </button>
                        </div>
                    </div>

                    <div class="total">
                        <div class="total__title">Итого к оплате: <span>11 500грн.</span></div>
                        <button class="btn">Оформить заказ</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h2 class="title-section">Спецпредложение</h2>

            <div class="special-offer">
                <div class="special-product">
                    <div class="special-product__img">
                        <img src="{{ asset('assets/images/product/product_main.png') }}" alt="">
                    </div>
                    <div>
                        <div class="special-product__title">
                            Наушники HyperX
                            Cloud Flight S Black
                        </div>

                        <div class="special-product__code">
                            Код товара: <b>4079100</b>
                        </div>
                    </div>
                </div>

                <div class="icon-add"></div>

                <div class="special-product-slider">
                    <div class="special-product">
                        <div class="special-product__img">
                            <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                        </div>
                        <div>
                            <div class="special-product__title">
                                Наушники HyperX
                                Cloud Flight S Black
                            </div>

                            <div class="special-product__code">
                                Код товара: <b>4079100</b>
                            </div>
                        </div>
                    </div>

                    <div class="special-product">
                        <div class="special-product__img">
                            <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                        </div>
                        <div>
                            <div class="special-product__title">
                                Наушники HyperX
                                Cloud Flight S Black
                            </div>

                            <div class="special-product__code">
                                Код товара: <b>4079100</b>
                            </div>
                        </div>
                    </div>

                    <div class="special-product">
                        <div class="special-product__img">
                            <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                        </div>
                        <div>
                            <div class="special-product__title">
                                Наушники HyperX
                                Cloud Flight S Black
                            </div>

                            <div class="special-product__code">
                                Код товара: <b>4079100</b>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="icon-equals"></div>

                <div class="special-product-price">
                    <div class="special-product-price__old">
                        250грн
                    </div>
                    <div class="special-product-price__new">937грн</div>

                    <button class="btn">Купить</button>
                </div>
            </div>
        </div>
    </section>

    <section class="section pb-5">
        <div class="container">
            <h2 class="title-section mb-3">Вместе покупают</h2>
            @yield('sidebar', View::make('client.partial.six_goods_slider'))
        </div>
    </section>
@endsection
