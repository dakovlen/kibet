@extends('layouts.client')

@section('content')
    <section class="breadcrumb-section">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/single-action">Kibet</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Акции</li>
                </ol>
            </nav>
        </div>
    </section>

    <section class="actions-container pb-5">
        <div class="container">
            <h2 class="title">Акции</h2>
            <div class="row">
                <div class="col-md-3 flex20">
                    <div class="filter mb-3 mb-md-4 mb-lg-5 mb-xl-5">
                        <div class="filter-mobile">
                            <i class="icon-filter filter-mobile__icon"></i>
                            Выбрать акции
                        </div>
                        <div class="filter-content">
                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse1"
                                        aria-expanded="false" aria-controls="collapse1">
                                    Цена
                                </button>
                                <div class="collapse" id="collapse1">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Товар в подарок</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Скидка</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Комплектом дешевле</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Кредит</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse2"
                                        aria-expanded="false" aria-controls="collapse2">
                                    Категории товаров
                                </button>
                                <div class="collapse" id="collapse2">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Категории 1</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Категории 2</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Категории 3</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Категории 4</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="filter-row">
                                <button class="filter__head" type="button" data-toggle="collapse" data-target="#collapse3"
                                        aria-expanded="false" aria-controls="collapse3">
                                    Производитель
                                </button>
                                <div class="collapse" id="collapse3">
                                    <div class="filter__content">
                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Asus</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>AsRock</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>A4Tech</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Be quiet!</span>
                                        </label>

                                        <label class="custom-checkbox">
                                            <input type="checkbox">
                                            <span>Canon</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-none d-lg-block d-xl-block">
                        @yield('sidebar', View::make('client.partial.sidebar'))
                    </div>
                </div>
                <div class="col-md-9 flex80">
                    <div class="row">
                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag action">скидка</span>
                                <img src="{{ asset('assets/images/blog/stocks1.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag present">подарок</span>
                                <img src="{{ asset('assets/images/blog/stocks2.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag credit">кредит</span>
                                <img src="{{ asset('assets/images/blog/stocks3.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag present">подарок</span>
                                <img src="{{ asset('assets/images/blog/stocks4.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Осталось <span class="red">2</span> дня</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag credit">кредит</span>
                                <img src="{{ asset('assets/images/blog/stocks5.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag cheaper-kit">Комплектом дешевле</span>
                                <img src="{{ asset('assets/images/blog/stocks6.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag cheaper-kit">Комплектом дешевле</span>
                                <img src="{{ asset('assets/images/blog/stocks2.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag present">Подарок</span>
                                <img src="{{ asset('assets/images/blog/stocks1.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-4">
                            <a href="/single-action" class="action">
                                <span class="tag discount">скидка</span>
                                <img src="{{ asset('assets/images/blog/stocks3.png') }}" alt="img"
                                     class="action__img">

                                <span class="action__title">К акционным SSD-накопителям Western Digital – сертификат</span>
                                <span class="action__info">
                                    <span class="action__left">Остался <span>21</span> день</span>
                                    <span class="action__period">
                                        <span> с 12 марта</span>
                                        <span>по 1 апреля</span>
                                    </span>
                                </span>
                            </a>
                        </div>

                        <div class="col-md-12">
                            <ul class="pagination mt-4 mb-4">
                                <li class="page-item"><a class="page-link" href="/single-action">1</a></li>
                                <li class="page-item active"><a class="page-link" href="/single-action">2</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">3</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">4</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">...</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action">32</a></li>
                                <li class="page-item"><a class="page-link" href="/single-action"> > </a></li>
                            </ul>
                        </div>

                        <div class="col-12 sidebar d-lg-none d-xl-none">
                            <div class="sidebar__row">
                                <div class="sidebar__title">
                                    <span>Подписаться на рассылку</span>
                                </div>
                                <div class="sidebar__mail">
                                    <input type="email" placeholder="Ваш E-mail">
                                    <i class="icon-mail icon"></i>
                                </div>
                            </div>
                            <div class="sidebar__row">
                                <div class="sidebar__title">
                                    <span>Мы в соц. сетях</span>
                                </div>
                                <div class="social">
                                    <a href="#" class="social__icon icon-fb"></a>
                                    <a href="#" class="social__icon icon-youtube"></a>
                                    <a href="#" class="social__icon icon-inst"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
