<div class="row">
    <div class="col-lg-9">
        <h2 class="title-section mt-4">Характеристики</h2>
        <div class="characteristics">
            <div class="characteristics__item">Тип наушников</div>
            <b class="characteristics__item">Проводные</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Наличие активного шумоподавления</div>
            <b class="characteristics__item">Гарнитура Bluetooth</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Цвет</div>
            <b class="characteristics__item">Grey</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Функции и возможности</div>
            <b class="characteristics__item">Встроенная звуковая карта</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Кабель</div>
            <b class="characteristics__item">Односторонний</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Импеданс, Ом</div>
            <b class="characteristics__item">60</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Диапазон частот наушников</div>
            <b class="characteristics__item">15 - 25 000 Гц</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Акустическое оформление</div>
            <b class="characteristics__item">Закрытые</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Чувствительность наушников</div>
            <b class="characteristics__item">98 дБ ± 3 дБ</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Максимальная входная мощность наушников</div>
            <b class="characteristics__item">150 мВт</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Тип микрофона</div>
            <b class="characteristics__item">Конденсаторный</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Конструкция микрофона</div>
            <b class="characteristics__item">Съемный</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Направленность микрофона</div>
            <b class="characteristics__item">Всенаправленный</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Диапазон частот микрофона</div>
            <b class="characteristics__item">50 - 18 000 Гц</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Чувствительность микрофона</div>
            <b class="characteristics__item">-39 ± 3 дБ</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Страна-производитель товара</div>
            <b class="characteristics__item">Китай</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Вес</div>
            <b class="characteristics__item">320г.</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Страна регистрации бренда</div>
            <b class="characteristics__item">США</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Гарантия</div>
            <b class="characteristics__item">24 месяца</b>
        </div>

        <div class="characteristics">
            <div class="characteristics__item">Интерфейс подключения</div>
            <b class="characteristics__item">3.5 мм (mini-Jack)</b>
        </div>
    </div>
    <div class="col-lg-3">
        @yield('product-sidebar', View::make('client.product_tab.product_sidebar'))
    </div>
</div>
