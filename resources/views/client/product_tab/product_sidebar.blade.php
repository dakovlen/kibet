<div class="product-sidebar">
    <div class="product-sidebar__top">
        <img src="{{ asset('assets/images/product/product.png') }}" alt="" class="product-sidebar__img">
        <div class="product-sidebar__title">
            Гарнитура Bluetooth Gemix
            BH-07 Gold, Bluetooth V3.0+
            EDR, накладные
        </div>
    </div>

    <div class="product-sidebar__info"> <!-- Add class inactive when have to show report block !-->
        <div class="product-sidebar__price">
            <span class="goods-price">
                <span class="price-action">
                    <span class="price-action__old">
                        520 <span>грн.</span>
                    </span>
                    <span class="price-action__new">
                        320 <span>грн.</span>
                    </span>
                </span>
            </span>

            <div class="product-sidebar__icon">
                <a href="#" class="icon-heart"></a>
                <a href="#" class="icon-balance"></a>
            </div>
        </div>

        <button class="product-sidebar__btn">
            <i class="icon-cart"></i>
            Купить
        </button>
        <!-- Show when parent has class inactive !-->
        <span class="report">
            <span>Нет в наличии</span>
            <span class="report__icon icon-mail"></span>
        </span>
    </div>
</div>
