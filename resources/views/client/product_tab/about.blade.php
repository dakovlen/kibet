<section class="section-about-tab">
    <div class="container">
        <div class="product-main-container row">

            <div class="col-lg-5 col-md-6">
                <div class="product-slider-box">
                    <div class="product-slider">
                        <div class="product-slider__item">
                            <div class="tag action">Акция</div>
                            <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                        </div>

                        <div class="product-slider__item">
                            <div class="tag action">Акция</div>
                            <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                        </div>

                        <div class="product-slider__item">
                            <div class="tag action">Акция</div>
                            <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                        </div>

                        <div class="product-slider__item">
                            <div class="tag bestseller">Акция</div>
                            <img src="{{asset('assets/images/product/product_dop.png')}}" alt="">
                        </div>

                        <div class="product-slider-main__item">
                            <div class="tag">Акция</div>
                            <img src="{{asset('assets/images/product/product_dop.png')}}" alt="">
                        </div>
                    </div>
                    <div class="product-slider-nav">
                    <div class="product-slider-nav__item">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                    </div>
                    <div class="product-slider-nav__item">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                    </div>
                    <div class="product-slider-nav__item">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                    </div>
                    <div class="product-slider-nav__item">
                        <img src="{{asset('assets/images/product/product_main.png')}}" alt="">
                    </div>
                </div>
                </div>
            </div>

            <div class="product-info col-lg-3 col-md-6">
                <div class="label">Есть в наличии</div>
                <div class="price">
                    <div class="price__old">937грн</div>
                    <div class="price__new">850грн</div>
                </div>
                <div>
                    <div class="product-info__btn">
                        <button class="btn">
                            <i class="icon-cart"></i>
                            Купить
                        </button>

                        <div>
                            <a href="#" class="icon icon-heart"></a>
                            <a href="#" class="icon icon-balance"></a>
                        </div>
                    </div>
                    <button class="btn btn-light">Купить в кредит</button>
                    <button class="btn btn-link">Купить в 1 клик</button>
                </div>
                <div class="product-info__share">
                    <span>Поделиться</span>
                    <div class="share">
                        <a href="#" class="share__link icon-fb"></a>
                        <a href="#" class="share__link icon-youtube"></a>
                        <a href="#" class="share__link icon-inst"></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-12">
                <div class="delivery">
                    <div class="delivery-row">
                        <div class="delivery__title">
                            <i class="icon-delivery "></i>
                            Способы доставки
                        </div>
                        <div class="delivery__content">
                            <div class="dropdown">
                                <span>Ваш город:</span>
                                <button class="dropdown-custom dropdown-city" type="button" id="dropdownMenuButton"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Запорожье
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Киев</a>
                                    <a class="dropdown-item" href="#">Днепр</a>
                                    <a class="dropdown-item" href="#">Харьков</a>
                                </div>
                            </div>
                            Самовывоз из точки выдачи – <span class="green">бесплатно</span>
                            <span class="marker"><i class="icon icon-marker"></i>просп. Маяковского, 4</span>
                            <p><b>Курьером</b> – от 40 грн</p>
                            <p><b>Нова Пошта</b> – по тарифам перевозчика</p>
                            <p><b>Ин Тайм</b> – по тарифам перевозчика</p>
                            <p><b>Delivery</b> – по тарифам перевозчика</p>
                        </div>
                    </div>

                    <div class="delivery-row">
                        <div class="delivery__title">
                            <i class="icon-credit"></i>
                            Способы оплаты
                        </div>
                        <div class="delivery__content">
                            Наличный расчет, безналичный расчет,
                            банковскими картами Visa/Mastercard, наложенный платеж, кредит
                        </div>
                    </div>

                    <div class="delivery-row">
                    <div class="delivery__title">
                        <i class="icon-security"></i>
                        Гарантия
                    </div>
                    <div class="delivery__content">
                        Срок гарантии: 12 мес.
                    </div>
                </div>
                </div>
            </div>
        </div>

    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section">Спецпредложение</h2>

        <div class="special-offer">
            <div class="special-product">
                <div class="special-product__img">
                    <img src="{{ asset('assets/images/product/product_main.png') }}" alt="">
                </div>
                <div>
                    <div class="special-product__title">
                        Наушники HyperX
                        Cloud Flight S Black
                    </div>

                    <div class="special-product__code">
                        Код товара: <b>4079100</b>
                    </div>
                </div>
            </div>

            <div class="icon-addition icon"></div>

            <div class="special-product-slider">
                <div class="special-product">
                    <div class="special-product__img">
                        <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                    </div>
                    <div>
                        <div class="special-product__title">
                            Наушники HyperX
                            Cloud Flight S Black
                        </div>

                        <div class="special-product__code">
                            Код товара: <b>4079100</b>
                        </div>
                    </div>
                </div>

                <div class="special-product">
                    <div class="special-product__img">
                        <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                    </div>
                    <div>
                        <div class="special-product__title">
                            Наушники HyperX
                            Cloud Flight S Black
                        </div>

                        <div class="special-product__code">
                            Код товара: <b>4079100</b>
                        </div>
                    </div>
                </div>

                <div class="special-product">
                    <div class="special-product__img">
                        <img src="{{ asset('assets/images/product/product_dop.png') }}" alt="">
                    </div>
                    <div>
                        <div class="special-product__title">
                            Наушники HyperX
                            Cloud Flight S Black
                        </div>

                        <div class="special-product__code">
                            Код товара: <b>4079100</b>
                        </div>
                    </div>
                </div>
            </div>

            <div class="icon-equals icon"></div>

            <div class="special-product-price">
                <div class="special-product-price__old">
                    250грн
                </div>
                <div class="special-product-price__new">937грн</div>

                <button class="btn">Купить комплект</button>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-4">Описание</h2>

        <p>
            В наше время выбор техники может стать непростой задачей. На данный момент существует широкий ассортимент
            моделей и целое множество производителей. При выборе той или иной модели советуем опираться на
            характеристики, которые для вас имеют приоритетное значение. Обратите внимание на Гарнитура, Гарнитура
            Gemix, Гарнитура Bluetooth Gemix BH-07 Gold, Bluetooth V3.0+EDR, накладные.
        </p>
        <p>
            Основные характеристики компьютерной, бытовой техники, расходных материалов и периферии на сайте
            интернет-магазина Кибет изложены в полном наименовании. Наши менеджеры с готовностью ответят на все
            возникшие у вас вопросы и помогут определиться с выбором.
        </p>
    </div>
</section>

<section class="section section-info">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12">
                <div class="characteristic-box">
                    <div class="characteristic-visible">
                        <h2 class="title-section">Характеристики</h2>
                        <div class="characteristics">
                            <div class="characteristics__item">Тип наушников</div>
                            <b class="characteristics__item">Проводные</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Наличие активного шумоподавления</div>
                            <b class="characteristics__item">Гарнитура Bluetooth</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Цвет</div>
                            <b class="characteristics__item">Grey</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Функции и возможности</div>
                            <b class="characteristics__item">Встроенная звуковая карта</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Кабель</div>
                            <b class="characteristics__item">Односторонний</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Импеданс, Ом</div>
                            <b class="characteristics__item">60</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Диапазон частот наушников</div>
                            <b class="characteristics__item">15 - 25 000 Гц</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Акустическое оформление</div>
                            <b class="characteristics__item">Закрытые</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Чувствительность наушников</div>
                            <b class="characteristics__item">98 дБ ± 3 дБ</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Максимальная входная мощность наушников</div>
                            <b class="characteristics__item">150 мВт</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Тип микрофона</div>
                            <b class="characteristics__item">Конденсаторный</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Конструкция микрофона</div>
                            <b class="characteristics__item">Съемный</b>
                        </div>

                        <a href="#" class="characteristics-link mt-3">Смотреть все характеристики</a>
                    </div>

                    <div class="characteristic-hide">

                        <div class="characteristics">
                            <div class="characteristics__item">Направленность микрофона</div>
                            <b class="characteristics__item">Всенаправленный</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Диапазон частот микрофона</div>
                            <b class="characteristics__item">50 - 18 000 Гц</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Чувствительность микрофона</div>
                            <b class="characteristics__item">-39 ± 3 дБ</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Страна-производитель товара</div>
                            <b class="characteristics__item">Китай</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Вес</div>
                            <b class="characteristics__item">320г.</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Страна регистрации бренда</div>
                            <b class="characteristics__item">США</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Гарантия</div>
                            <b class="characteristics__item">24 месяца</b>
                        </div>

                        <div class="characteristics">
                            <div class="characteristics__item">Интерфейс подключения</div>
                            <b class="characteristics__item">3.5 мм (mini-Jack)</b>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12">
                <div class="feedback">
                    <div class="feedback-header">
                        <h2 class="title-section">Оставить отзыв</h2>
                        <div class="enter-through">
                            Войти через:
                            <a href="#" class="enter-through__link icon-google"></a>
                            <a href="#" class="enter-through__link icon-fb"></a>
                        </div>
                    </div>
                    <div class="feedback__form row">
                        <div class="col-md-12 mb-3">Пока нет отзывов. Будьте первым, кто оставит его.</div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Ваш e-mail">
                        </div>
                        <div class="col-md-12 mt-2 mb-3">
                            <input id="input-id" name="input-name" type="number" class="rating" min=1 max=5 step=1
                                   data-size="md" data-rtl="false">
                        </div>

                        <div class="col-md-12 mb-3">
                            <input type="text" placeholder="Достоинства">
                        </div>

                        <div class="col-md-12 mb-3">
                            <input type="text" placeholder="Недостатки">
                        </div>

                        <div class="col-md-12 mb-3">
                            <textarea cols="30" rows="6" placeholder="Комментарий"></textarea>
                        </div>

                        <div class="col-md-12 text-center">
                            <button class="btn mt-4 mb-3">Оставить отзыв</button>

                            <p>Уже есть аккаунт? <a href="#">Войти под своим именем</a></p>
                            <p>
                                Важно! Чтобы Ваш отзыв прошел модерацию и был опубликован,
                                рекомендуем ознакомиться с <a href="">Правилами</a>
                            </p>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-4">Обзоры</h2>

        <div class="row">
            <div class="col-lg-4 col-md-6">
                <a href="#" class="reviews">
                    <span class="reviews__iframe">
                        <iframe width="100%" height="215" src="https://www.youtube.com/embed/1Jw_60qW_TQ"
                                frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </span>
                    <span class="reviews__title">
                        Гарнитура Bluetooth Gemix BH-07 Gold, Bluetooth
                        V3.0+EDR,накладные
                    </span>
                </a>
            </div>

            <div class="col-lg-4 col-md-6">
                <a href="#" class="reviews">
                        <span class="reviews__iframe">
                            <iframe width="100%" height="215" src="https://www.youtube.com/embed/1Jw_60qW_TQ"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </span>
                    <span class="reviews__title">
                            Гарнитура Bluetooth Gemix BH-07 Gold, Bluetooth
                            V3.0+EDR,накладные
                        </span>
                </a>
            </div>

            <div class="col-lg-4 d-none d-lg-block d-xl-block">
                <a href="#" class="reviews">
                        <span class="reviews__iframe">
                            <iframe width="100%" height="215" src="https://www.youtube.com/embed/1Jw_60qW_TQ"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                        </span>
                    <span class="reviews__title">
                            Гарнитура Bluetooth Gemix BH-07 Gold, Bluetooth
                            V3.0+EDR,накладные
                        </span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-4">Вместе покупают</h2>
        @yield('sidebar', View::make('client.partial.six_goods_slider'))
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-4">Аналоги</h2>
        @yield('sidebar', View::make('client.partial.six_goods_slider'))
    </div>
</section>
