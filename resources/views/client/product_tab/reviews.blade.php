    <div class="row">
    <div class="col-lg-9 review-container">
        <div class="review-head">
            <h2 class="title-section"> Отзывы покупателей (5)</h2>

            <a href="#" class="btn">Оставить отзыв</a>
        </div>

        <div class="review">
            <div class="review__head">
                <div class="review__title">Вася Петечкин</div>
                <div class="review__date">12 марта 2020</div>
            </div>
            <div class="review__text">
                Через пару дней после покупки пропал звук на динамиках, вместо звука теперь слышны тихие
                щелчки. В наушниках звук есть. Что делать не знаю, все драйвера переустановил и
                диагностики которые Виндоус предлагает сделал, пишет все ок, а динамики не работают.
                Осталось только ОС переустановить, если не появится то буду думать о возврате
            </div>
            <div class="review__bottom">
                <div class="answer-box">
                    <a href="#" class="answer">Ответить</a>
                    <div>5 ответов</div>
                </div>
                <div class="like-box">
                    <a href="#" class="like-link like">
                        <i class="icon-like"></i>
                        5
                    </a>
                    <a href="#" class="like-link dislike">
                        <i class="icon-dislike"></i>
                        5
                    </a>
                </div>
            </div>

            <form class="review__form">
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" placeholder="Ваше имя">
                    </div>
                    <div class="col-md-6">
                        <input type="email" placeholder="Ваш e-mail">
                    </div>

                    <div class="col-md-12">
                        <textarea cols="30" rows="7" placeholder="Ваш ответ"></textarea>
                    </div>

                    <div class="col-md-12">
                        <button class="btn">Добавить</button>
                    </div>
                </div>
            </form>

            <div class="review-list">
                <div class="review-list__visible">
                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Ростислав</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">После 2-х лет использования слезла кожа с амбушур и дужки. Думаю что с этим сделать и где заменить</div>
                    </div>

                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Константин Черный</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">Качество микрофона можно оценить по видеообзору</div>
                    </div>

                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Витович Евгений</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">На микрофон никто не жаловался из знакомых</div>
                    </div>
                    <div class="text-center">
                        <a href="#" class="review-list__link">Читать все отзывы</a>
                    </div>
                </div>

                <div class="review-list__hide">
                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Ростислав</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">После 2-х лет использования слезла кожа с амбушур и дужки. Думаю что с этим сделать и где заменить</div>
                    </div>

                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Константин Черный</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">Качество микрофона можно оценить по видеообзору</div>
                    </div>

                    <div class="review">
                        <div class="review__head">
                            <div class="review__title">Витович Евгений</div>
                            <div class="review__date">12 марта 2020</div>
                        </div>
                        <div class="review__text">На микрофон никто не жаловался из знакомых</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="review">
            <div class="review__head">
                <div class="review__title">Вася Петечкин</div>
                <div class="review__date">12 марта 2020</div>
            </div>
            <div class="review__text">
                Через пару дней после покупки пропал звук на динамиках, вместо звука теперь слышны тихие
                щелчки. В наушниках звук есть. Что делать не знаю, все драйвера переустановил и
                диагностики которые Виндоус предлагает сделал, пишет все ок, а динамики не работают.
                Осталось только ОС переустановить, если не появится то буду думать о возврате

                <div class="review__text--info">
                    <b>Достоинства</b>
                    хорошие за свои деньги
                </div>

                <div class="review__text--info">
                    <b>Недостатки</b>
                    Возникли проблемы с динамиками
                </div>
            </div>
            <div class="review__bottom">
                <div class="answer-box">
                    <a href="#" class="answer">Ответить</a>
                    <div>5 ответов</div>
                </div>
                <div class="like-box">
                    <a href="#" class="like-link like">
                        <i class="icon-like"></i>
                        5
                    </a>
                    <a href="#" class="like-link dislike">
                        <i class="icon-dislike"></i>
                        5
                    </a>
                </div>
            </div>
        </div>

        <div class="review">
            <div class="review__head">
                <div class="review__title">Вася Петечкин</div>
                <div class="review__date">12 марта 2020</div>
            </div>

            <div class="rating-star three">
                <div class="icon-star"></div>
                <div class="icon-star"></div>
                <div class="icon-star"></div>
                <div class="icon-star"></div>
                <div class="icon-star"></div>
            </div>

            <div class="review__text">
                Через пару дней после покупки пропал звук на динамиках, вместо звука теперь слышны тихие
                щелчки. В наушниках звук есть. Что делать не знаю, все драйвера переустановил и
                диагностики которые Виндоус предлагает сделал, пишет все ок, а динамики не работают.
                Осталось только ОС переустановить, если не появится то буду думать о возврате

                <div class="review__text--info">
                    <b>Достоинства</b>
                    хорошие за свои деньги
                </div>

                <div class="review__text--info">
                    <b>Недостатки</b>
                    Возникли проблемы с динамиками
                </div>
            </div>
            <div class="review__bottom">
                <div class="answer-box">
                    <a href="#" class="answer">Ответить</a>
                    <div>5 ответов</div>
                </div>
                <div class="like-box">
                    <a href="#" class="like-link like">
                        <i class="icon-like"></i>
                        5
                    </a>
                    <a href="#" class="like-link dislike">
                        <i class="icon-dislike"></i>
                        5
                    </a>
                </div>
            </div>
        </div>

        <div class="text-center mt-4 mb-4">
            <a href="#" class="more">Все отзывы <span>13</span>  </a>
        </div>
    </div>

    <div class="col-lg-3">
            <div class="product-sidebar">
                <div class="product-sidebar__top">
                    <img src="{{ asset('assets/images/product/product.png') }}" alt="" class="product-sidebar__img">
                    <div class="product-sidebar__title">
                        Гарнитура Bluetooth Gemix
                        BH-07 Gold, Bluetooth V3.0+
                        EDR, накладные
                    </div>
                </div>

                <div class="product-sidebar__info inactive"> <!-- Add class inactive when have to show report block !-->
                    <div class="product-sidebar__price">
            <span class="goods-price">
                <span class="price-action">
                    <span class="price-action__old">
                        520 <span>грн.</span>
                    </span>
                    <span class="price-action__new">
                        320 <span>грн.</span>
                    </span>
                </span>
            </span>

                        <div class="product-sidebar__icon">
                            <a href="#" class="icon-heart"></a>
                            <a href="#" class="icon-balance"></a>
                        </div>
                    </div>

                    <button class="product-sidebar__btn">
                        <i class="icon-cart"></i>
                        Купить
                    </button>
                    <!-- Show when parent has class inactive !-->
                    <span class="report">
            <span>Нет в наличии</span>
            <span class="report__icon icon-mail">
                <span class="text-mobile">Сообщить о наличии</span>
            </span>
        </span>
                </div>
            </div>

        </div>
</div>
