<section class="section">
    <div class="container">
        <h2 class="title-section mb-3">
            Категория 1
        </h2>
        @yield('slider', View::make('client.partial.six_goods_slider'))
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-3">
            Категория 2
        </h2>
        @yield('slider', View::make('client.partial.six_goods_slider'))
    </div>
</section>

<section class="section">
    <div class="container">
        <h2 class="title-section mb-3">
            Категория 3
        </h2>
        @yield('slider', View::make('client.partial.six_goods_slider'))
    </div>
</section>
