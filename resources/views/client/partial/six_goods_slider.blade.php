<div class="viewed-slider base-slider">
    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <span class="tag action">Акция</span>
                <img src="{{ asset('assets/images/product/product.png') }}" alt="goods img"
                     class="goods__img">
                <span class="goods__title">Смартфон Huawei P Smart Z Sapphire</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <span class="tag super-price">суперцена</span>
                <img src="{{ asset('assets/images/laptop.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Ноутбук Lenovo IdeaPad S540 15IWL (81NE00CJRA)</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">

                <img src="{{ asset('assets/images/product/product3.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Водонагреватель проточный Holmer</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <img src="{{ asset('assets/images/product/product4.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Блендер Xiaomi Viomi 350 мл (VBH129)</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <img src="{{ asset('assets/images/product/product5.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Блендер TEFAL QUICKCHEF HB653838</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>

    <div class="goods-box">
        <a href="/product" class="goods">
            <span class="goods-visible">
                <img src="{{ asset('assets/images/product/product6.png') }}" alt="goods img"
                     class="goods__img">

                <span class="goods__title">Кухонный комбайн PHILIPS Avance HR7778/00</span>

                <span class="goods-actions">
                    <span class="goods__icon">
                        <i class="icon-heart"></i>
                    </span>
                    <span class="goods__icon">
                        <i class="icon-balance"></i>
                    </span>
                </span>

                <span class="goods-price">
                    <span class="price-action">
                        <span class="price-action__new">
                            320 <span>грн.</span>
                        </span>
                        <span class="price-action__old">
                            520 <span>грн.</span>
                        </span>
                    </span>
                    <span class="goods-price__cart icon-cart"></span>
                </span>
            </span>
        </a>
    </div>
</div>
