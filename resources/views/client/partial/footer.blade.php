<footer class="footer-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 footer__item d-none d-md-block d-lg-block d-xl-block">
                <ul class="footer-menu">
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Оплата и доставка</a></li>
                    <li><a href="#">Гарантия и сервис</a></li>
                    <li><a href="#">Контакты</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer__item d-none d-md-block d-lg-block d-xl-block">
                <ul class="footer-menu">
                    <li><a href="#">Блог</a></li>
                    <li><a href="#">Акции</a></li>
                    <li><a href="#">Спецпредложения</a></li>
                    <li><a href="#">Условия использования сайта</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer__item">
                <div class="work-time">
                    <div>
                        <div>пн-пт: 09:00 - 18:00</div>
                        <div>суббота: 09:00 - 15:00</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 footer__item">
                <div class="tel-box">
                    <div>
                        <a href="mailto:savchenko_on@kibet.com" class="mail">savchenko_on@kibet.com</a>
                        <div class="dropdown">
                            <button class="dropdown-custom dropdown-tel mb-1" type="button" id="dropdownTelBox"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                +38 (061) 707-14-67
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownTelBox">
                                <a class="dropdown-item" href="#">(068)33-89-288</a>
                                <a class="dropdown-item" href="#">(096)33-89-288</a>
                            </div>
                        </div>
                        <a href="" class="">Заказать звонок</a></div>
                </div>

                <div class="contact-footer">
                   <div>
                       <a href="mailto:savchenko_on@kibet.com" class="mail">savchenko_on@kibet.com</a>
                       <a href="tel:+380617071467">+38 (061) 707-14-67</a>
                   </div>
                    <div class="social">
                        <a href="#" class="social__icon icon-fb"></a>
                        <a href="#" class="social__icon icon-youtube"></a>
                        <a href="#" class="social__icon icon-inst"></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            © Все права защищены. 2017 «Kibet»
        </div>
    </div>
</footer>
