<header class="header-content">

    <div class="container">
        <div class="header-top row">
            <a href="./" class="logo col-md-3">
                <img src="{{ asset('assets/images/logo.png') }}" alt="logo">
            </a>
            <div class="col-md-9">
                <div class="nav-box">
                    <div class="lang">
                        <div class="dropdown">
                            <button class="dropdown-custom" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                RU
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="#">EN</a>
                                <a class="dropdown-item" href="#">UK</a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="login">
                        <span class="icon-login login__icon"></span>
                        <span>Вход</span>
                    </a>
                </div>
                <ul class="header-menu">
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Оплата и доставка</a></li>
                    <li><a href="#">Гарантия и сервис</a></li>
                    <li><a href="#">Контакты</a></li>
                    <li><a href="/news">Блог</a></li>
                    <li><a href="/action">Акции</a></li>
                    <li><a href="#">Спецпредложения</a></li>
                </ul>
            </div>
        </div>

        <div class="header-top-mobile">
            <div class="menu-mobile">
                <div class="menu-mobile__head">
                    <a href="#" class="login">
                        <span class="icon-login login__icon"></span>
                        <span>Вход</span>
                    </a>

                    <span class="mr-2 ml-2">|</span>

                    <a href="#">Регистрация</a>
                </div>
                <div class="menu-mobile__content">
                    <ul>
                        <li>
                            <a href="">
                                <span class="icon-heart menu-mobile__icon"></span>
                                Список желаний
                            </a>
                        </li>

                        <li>
                            <a href="">
                                <span class="icon-balance menu-mobile__icon"></span>
                                Сравнение
                            </a>
                        </li>
                        <li class="border-menu"></li>
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Каталог</a></li>
                        <li><a href="#">Оплата и доставка</a></li>
                        <li><a href="#">Гарантия и сервис</a></li>
                        <li><a href="#">Контакты</a></li>
                        <li>
                            <div class="tel-box">
                                <a href="tel:0673258650">0673258650</a>
                            </div>
                        </li>

                        <li>
                            <div class="tel-box">
                                <a href="tel:0502559520">0502559520</a>
                            </div>
                        </li>
                        <li class="border-menu"></li>
                        <li><a href="/action">Акции</a></li>
                        <li><a href="/news">Блог</a></li>
                    </ul>

                </div>
                <div class="menu-mobile__bottom">
                    <a href="#">
                        <i class="icon-desktop"></i>
                        Перейти на полную версию сайта
                    </a>
                </div>
            </div>

            <div class="d-flex align-items-center justify-content-between">
                <div class="menu-icon-box">
                    <div class="menu-icon"></div>
                </div>

                <a href="./" class="logo">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="logo">
                </a>
            </div>

            <div class="d-flex align-items-center justify-content-between">
                <div class="search-box">
                    <div class="search-mobile">
                        <span class="icon-search"></span>
                    </div>
                    <div class="input-search-box">
                        <input type="text" class="search-box__input" placeholder="Поиск">
                        <span class="icon-search search-box__icon"></span>
                    </div>
                </div>

                <div class="lang">
                    <div class="dropdown">
                        <button class="dropdown-custom" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            RU
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">EN</a>
                            <a class="dropdown-item" href="#">UK</a>
                        </div>
                    </div>
                </div>

                <div class="cart-box">
                    <a href="#" class="cart-box__link cart">
                        <span class="icon-cart"></span>
                        <span class="cart__counter">3</span>
                    </a>
                </div>
            </div>
        </div>

        <div class="index-slider-mobile">
            <div class="index-slider">
                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>

                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>

                <div class="index-slider__item">
                    <img src="{{ asset('assets/images/slider.png') }}" alt="img">
                </div>
            </div>
        </div>

        <div class="header-bottom">
            <div class="category-select">
                <div class="dropdown dropdown-category">
                    <button class="dropdown-category-btn" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Каталог товаров
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Ноутбуки и планшеты
                            </a>
                            <div class="dropdown-menu-content">
                                <div class="dropdown-menu-content__item">
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                    </ul>
                                </div>

                                <div class="dropdown-menu-content__item">
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                    </ul>
                                </div>

                                <div class="dropdown-menu-content__item">
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                    </ul>
                                </div>

                                <div class="dropdown-menu-content__item">
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                        <li><a href="#">Ноутбуки и планшеты2</a></li>
                                        <li><a href="#">Ноутбуки и планшеты3</a></li>
                                    </ul>
                                    <ul>
                                        <li class="list-title"><a href="#">Ноутбуки и планшеты</a></li>
                                        <li><a href="#">Ноутбуки и планшеты1</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Комплектующие для ПК
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Носители информации
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Сетевое оборудование
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                ТВ, Аудио, Фото
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Техника для дома
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Расходники
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Периферия
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Телефония
                            </a>
                        </li>

                        <li class="dropdown-item dropdown-category__item">
                            <a href="/category" class="dropdown-category__link">
                                Все категории
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="tel-box">
                <div>
                    <div>
                        <!-- Button trigger modal -->
                        <button type="button" class="dropdown-custom dropdown-tel mb-1" data-toggle="modal"
                                data-target="#exampleModal">
                            (050)33-89-288
                        </button>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Модальное окно</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <a href="#" class="mb-2">(068)33-89-288</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <!-- Button trigger modal -->
                        <a href="#" data-toggle="modal"
                           data-target="#exampleModal2">Заказать звонок</a>

                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Заказать звонок</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="#">
                                            <input type="text" placeholder="Ваш имя" class="mb-2">
                                            <input type="tel" placeholder="Ваш номер" class="mb-2">
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отправить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="search-box">
                <div class="search-mobile">
                    <span class="icon-search"></span>
                </div>
                <div class="input-search-box">
                    <input type="text" class="search-box__input" placeholder="Поиск">
                    <span class="icon-search search-box__icon"></span>
                </div>
            </div>
            <div class="cart-box">
                <a href="#" class="cart-box__link like-btn">
                    <span class="icon-heart"></span>
                </a>
                <a href="#" class="cart-box__link comparison">
                    <span class="icon-balance"></span>
                </a>
                <a href="#" class="cart-box__link cart">
                    <span class="icon-cart"></span>
                    <span class="cart__counter">3</span>
                </a>
            </div>
        </div>
    </div>
</header>



