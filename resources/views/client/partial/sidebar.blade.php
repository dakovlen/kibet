<div class="sidebar">
    <div class="sidebar__row">
        <div class="sidebar__title">
            <span>Подписаться на рассылку</span>
        </div>
        <div class="sidebar__mail">
            <input type="email" placeholder="Ваш E-mail">
            <i class="icon-mail icon"></i>
        </div>
    </div>
    <div class="sidebar__row">
        <div class="sidebar__title">
            <span>Мы в соц. сетях</span>
        </div>
        <div class="social">
            <a href="#" class="social__icon icon-fb"></a>
            <a href="#" class="social__icon icon-youtube"></a>
            <a href="#" class="social__icon icon-inst"></a>
        </div>
    </div>
</div>
