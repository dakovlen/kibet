<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ Config::get('app.name') }}</title>
{{--<link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.svg') }}" type="image/x-icon">--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>

    <!-- Styles -->
    <link href="{{ URL::asset('assets/css/app.css') }}" rel="stylesheet">
</head>

<body @if (isset($bodyClass)) class="{{$bodyClass}}" @endif>
<div id="app">
    @yield('header', View::make('client.partial.header'))
    <main class="main">
        @yield('breadcrumbs')
        @yield('content')
    </main>
    @yield('footer', View::make('client.partial.footer'))
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="{{ URL::asset('assets/js-lib/accordion.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script src="{{ URL::asset('assets/js/app.js') }}"></script>
</body>
</html>
