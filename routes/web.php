<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view("client/home", ["bodyClass"=>'home-page']);
});

Route::get('/news', function () {
    return view('client/news', ["bodyClass"=>'news-page']);
});

Route::get('/single-news', function () {
    return view('client/single_news', ["bodyClass"=>'article-page']);
});

Route::get('/catalog', function () {
    return view('client/catalog', ["bodyClass"=>'catalog-page']);
});

Route::get('/category', function () {
    return view('client/category', ["bodyClass"=>'category-page']);
});

Route::get('/action', function () {
    return view('client/action', ["bodyClass"=>'action-page']);
});

Route::get('/single-action', function () {
    return view('client/single_action', ["bodyClass"=>'single-action-page']);
});

Route::get('/product', function () {
    return view('client/product');
});

Route::get('/card', function () {
    return view('client/card');
});
